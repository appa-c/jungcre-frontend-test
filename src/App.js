import React, { Component } from 'react';
import './App.css';

import FbLogin from './fb-login/fb-login';
import FbProfile from './fb-profile/fb-profile';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fbData: null
    };
  }

  handleLogin = (response) => {
    if (response.accessToken) {
      this.setState({
        fbData: response
      });
    }
  }

  render() {
    if (!this.state.fbData) {
      return(
        <FbLogin loginCallback={this.handleLogin}></FbLogin>
      );
    } else {
      return (
        <FbProfile fbData={this.state.fbData}></FbProfile>
      );
    }
  }
}

export default App;
