import React, { Component } from 'react';
import './fb-profile.css';

const getColours = require('get-image-colors');

class ColourPalette extends Component {
    constructor(props) {
        super(props);
        this.state = {
            palette: null
        };
    }

  render() {
    getColours(this.props.image)
        .then(colours => {
            this.setState({
                palette: colours.map(colour => colour.hex())
            });
        })
        .catch(error => {
            console.log(error);
        })

    if (this.state.palette) {
        const paletteItems = this.state.palette.map(colour => {
            const itemStyle = {
                backgroundColor: `${colour}`
            }
            return <li className="palette__list_item" style={itemStyle} key={colour}>{colour}</li>
        });
        return (
            <ul className="palette_list">{paletteItems}</ul>
        )
    } else {
        return (
            <small>Loading palette...</small>
        );
    }
  }
}

export default ColourPalette;
