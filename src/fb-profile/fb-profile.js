import React, { Component } from 'react';
import ColourPalette from './palette';
import'./fb-profile.css';

class FbProfile extends Component {
  render() {
    const profileStyle = {
      backgroundImage: `url(${this.props.fbData.picture.data.url})`
    };

    return (
        <div className="profile">
          <div className="profile__image" style={profileStyle}></div>
          <div className="profile__summary">
            <h1>hey, {this.props.fbData.first_name}</h1>
            <p>Here's the colour palette for your Facebook profile picture...</p>
            <div className="profile__palette">
                <ColourPalette image={this.props.fbData.picture.data.url}></ColourPalette>
            </div>
          </div>
        </div>
    );
  }
}

export default FbProfile;
