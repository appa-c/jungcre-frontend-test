import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import './fb-login.css';

class FbLogin extends Component {
  render() {
    return (
      <div className="login_button">
        <FacebookLogin
            appId="150477578945957"
            autoLoad={false}
            fields="first_name,picture"
            callback={this.props.loginCallback}
        />
      </div>
    );
  }
}

export default FbLogin;
