# Jungle Creations Frontend Test

A react app for getting the colour palette of a user's Facebook profile picture.

## Getting Started

Clone this repository and run the app locally using `npm start`.

Alternatively, a live version of the app is available at `https://fb-profile-picture-pal.firebaseapp.com`.

## Usage

The app requires you to authenticate via Facebook. Once done, your facebook profile picture is passed to [get-image-colors](https://yarnpkg.com/en/package/get-image-colors), the results of which are displayed to you.

## Development

This app was scaffolded using [create-react-app](https://github.com/facebookincubator/create-react-app).
